exports.events = [
  // when file content is changing
  'change',

  // when file was deleted
  'unlink',

  // when directory was removed
  'unlinkDir',
]

exports.callback = (event, pid) => () => {
  process.kill(pid, 'SIGINT')
  process.exit()
}

const child = require('child_process')
const chokidar = require('chokidar')
const pkg = require('../package.json')
const {log} = require('./log')
const {events, callback} = require('./events')

function die(pattern, code = 0) {
  log.green('[Naka] Unsubscribing from', `"${pattern}"`, `(${code})`, '...')
  process.exit(code)
}

function help() {
  console.log(`Usage: ${pkg.name} <file_to_exec> <watch_pattern>`)
  console.log(`Example: ${pkg.name} server/server.js server/**/*`)
  process.exit(1)
}

function main(argv) {
  if (argv.length !== 2)
    return help()

  const [file_to_exec, watch_pattern] = argv

  const node = child.spawn('node', [file_to_exec])
  log.green('[Naka] Watching', `"${watch_pattern}"`, '...')

  node.stdout.pipe(process.stdout)
  node.stderr.pipe(process.stderr)

  node.on('close', code => {
    die(file_to_exec, code)
    watcher.close()
  })

  const watcher = chokidar.watch(watch_pattern)

  events.forEach(event => {
    watcher.on(event, callback(event, node.pid))
  })

  process.on('SIGINT', () => {
    watcher.close()
  })
}

module.exports = {main}

const chalk = require('chalk')

const log = {
  green: (...x) => console.log(chalk.green(...x)),
}

module.exports = {log}

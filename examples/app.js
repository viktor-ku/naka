const sleep = (timeout) => new Promise((resolve) => {
  setTimeout(resolve, timeout)
})

const global_time_ratio = Math.random() * 10
const chance_ratio = 0.9551
const columns = 64
const rows = 32

function dot() {
  let char = '.'

  if (Math.random() > chance_ratio)
    char = '0'
  else if (Math.random() > chance_ratio)
    char = 'x'
  else if (Math.random() > chance_ratio)
    char = '#'
  else if (Math.random() > chance_ratio)
    char = 'Y'

  process.stdout.write(char)
}

function eol() {
  process.stdout.write('\n')
}

async function main() {
  for (let row_i = 0; row_i < rows; row_i += 1) {
    const timeout = Math.floor(Math.random() * global_time_ratio * 100)

    for (let col_i = 0; col_i < columns; col_i += 1) {
      await sleep(timeout)
      dot()
    }

    eol()
  }
}

main()

process.on('SIGINT', () => {
  console.log('')
  process.exit(0)
})

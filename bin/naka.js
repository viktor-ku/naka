#!/bin/env node

const {main} = require('../src/main.js')

try {
  main(process.argv.slice(2))
} catch (e) {
  console.error(e.message)
  process.exit(1)
}
